
package com.example.sqlite;

import android.app.ListActivity;
import android.app.LoaderManager;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

public class MyActivity extends ListActivity implements AdapterView.OnItemClickListener,
        LoaderManager.LoaderCallbacks<Cursor> {

    private static final String ARG_URI = "uri_arg";

    private static final String SORT_ORDER = FileBrowserContract.File.Columns.TYPE
            + ", LOWER(" + FileBrowserContract.File.Columns.TITLE + ")";

    private MyAdapter mAdapter;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mAdapter = new MyAdapter(this);

        setListAdapter(mAdapter);
        getListView().setOnItemClickListener(this);

        Bundle args = new Bundle(1);
        args.putString(ARG_URI, FileBrowserContract.File.CONTENT_URI.toString());
        LoaderManager loaderManager = getLoaderManager();
        loaderManager.initLoader(0, args, this);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Cursor cursor = (Cursor) getListAdapter().getItem(i);
        int type = cursor.getInt(cursor.getColumnIndex(FileBrowserContract.File.Columns.TYPE));
        if (FileBrowserContract.File.Type.DIRECTORY != type) {
            return;
        }

        String path = cursor
                .getString(cursor.getColumnIndex(FileBrowserContract.File.Columns.PATH));

        Bundle args = new Bundle(1);
        args.putString(ARG_URI, Uri.withAppendedPath(FileBrowserContract.File.CONTENT_URI,
                Uri.encode(path.substring(1))).toString());
        getLoaderManager().restartLoader(0, args, this);
    }

    @Override
    public void onBackPressed() {
        ListView listView = getListView();
        Cursor cursor = (Cursor) listView.getItemAtPosition(0);
        String title = cursor.getString(cursor
                .getColumnIndex(FileBrowserContract.File.Columns.TITLE));
        if ("..".equals(title)) {
            onItemClick(listView, null, 0, 0);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String uri = args.getString(ARG_URI);
        return new CursorLoader(this, SQLiteProviderContract.getContentUri(uri), null, null,
                null, SORT_ORDER);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mAdapter.changeCursor(data);
        getListView().setSelection(0);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapter.changeCursor(null);
    }

    private static class MyAdapter extends CursorAdapter {

        private int mTitleIdx;
        private int mTypeIdx;
        private int mSizeIdx;

        private MyAdapter(Context context) {
            super(context, null, false);
        }

        @Override
        public void changeCursor(Cursor cursor) {
            setIdx(cursor);
            super.changeCursor(cursor);
        }

        private void setIdx(Cursor cursor) {
            if (cursor != null) {
                mTitleIdx = cursor.getColumnIndex(FileBrowserContract.File.Columns.TITLE);
                mTypeIdx = cursor.getColumnIndex(FileBrowserContract.File.Columns.TYPE);
                mSizeIdx = cursor.getColumnIndex(FileBrowserContract.File.Columns.SIZE);
            }
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.list_item, viewGroup, false);
            Content content = new Content();
            content.mIcon = (ImageView) view.findViewById(R.id.icon);
            content.mTitle = (TextView) view.findViewById(R.id.title);
            content.mSize = (TextView) view.findViewById(R.id.size);

            view.setTag(content);

            return view;
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            Content content = (Content) view.getTag();

            content.mTitle.setText(cursor.getString(mTitleIdx));
            if (cursor.getInt(mTypeIdx) == FileBrowserContract.File.Type.DIRECTORY) {
                content.mIcon.setImageResource(R.drawable.folder);
                content.mSize.setText("");
                content.mSize.setVisibility(View.GONE);
            } else {
                content.mIcon.setImageResource(R.drawable.file);
                content.mSize.setText(String.valueOf(cursor.getInt(mSizeIdx)));
                content.mSize.setVisibility(View.VISIBLE);
            }
        }

        private static final class Content {
            private TextView mTitle;
            private TextView mSize;
            private ImageView mIcon;
        }
    }
}
