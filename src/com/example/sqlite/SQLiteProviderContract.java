
package com.example.sqlite;

import android.net.Uri;

/**
 * The contract for SQLiteProvider
 */
public final class SQLiteProviderContract {
    /**
     * The authority for SQLiteProvider
     */
    public static final String AUTHORITY = "com.example.sqlite_provider";

    /**
     * A content:// style uri to the authority for SQLiteProvider
     */
    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY);

    public static final String PARAM_URI = "uri";

    private SQLiteProviderContract() {
    }

    public static Uri getContentUri(String origUri) {
        Uri.Builder uriBuilder = SQLiteProviderContract.CONTENT_URI.buildUpon();
        return uriBuilder.appendQueryParameter(SQLiteProviderContract.PARAM_URI, origUri).build();
    }
}
