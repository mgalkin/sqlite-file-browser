package com.example.sqlite;

import android.net.Uri;

/**
 * The contract between File Browser and applications. Contains definitions for
 * the supported URIs and data columns.
 */
public final class FileBrowserContract {
    /**
     * The authority for File Browser
     */
    public static final String AUTHORITY = "com.example.sqlite.filebrowser";

    /**
     * A content:// style uri to the authority for File Browser
     */
    public static final Uri AUTHORITY_URI = Uri.parse("content://" + AUTHORITY);

    private FileBrowserContract() {
    }

    public static final class File {
        /**
         * Table name for files.
         */
        public static final String TABLE_NAME = "files";

        /**
         * Content URI for files.
         */
        public static final Uri CONTENT_URI = Uri.withAppendedPath(AUTHORITY_URI, TABLE_NAME);

        private File() {
        }

        /**
         * Table columns
         */
        public static final class Columns {

            /**
             * Title (filename) - String
             */
            public static final String TITLE = "title";

            /**
             * File Type - Integer. See {@link Type}
             */
            public static final String TYPE = "type";

            /**
             * File size - Integer
             */
            public static final String SIZE = "size";

            /**
             * Full path - String.
             */
            public static final String PATH = "path";

            private Columns() {
            }
        }

        /**
         * Possible file types
         */
        public static final class Type {
            /**
             * Type directory
             */
            public static final int DIRECTORY = 0;

            /**
             * Type file
             */
            public static final int FILE = 1;

            private Type() {
            }
        }
    }
}
