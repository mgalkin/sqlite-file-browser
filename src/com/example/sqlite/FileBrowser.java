
package com.example.sqlite;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;

import java.io.File;

/**
 * ContentProvider for File Browser.
 */
public class FileBrowser extends ContentProvider {

    private static final UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    private static final int FILE_LIST_ROOT = 0;
    private static final int FILE_LIST_DIR = 1;

    private static final String ROOT_DIR = "/";

    private static final String[] ALL_COLUMNS = {
            FileBrowserContract.File.Columns.TITLE,
            FileBrowserContract.File.Columns.PATH,
            FileBrowserContract.File.Columns.SIZE,
            FileBrowserContract.File.Columns.TYPE,
    };

    static {
        sUriMatcher.addURI(FileBrowserContract.AUTHORITY, FileBrowserContract.File.TABLE_NAME,
                FILE_LIST_ROOT);
        sUriMatcher.addURI(FileBrowserContract.AUTHORITY, FileBrowserContract.File.TABLE_NAME
                + "/*",
                FILE_LIST_DIR);
    }

    @Override
    public boolean onCreate() {
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs,
            String sortOrder) {
        String path;
        switch (sUriMatcher.match(uri)) {
            case FILE_LIST_ROOT:
                path = ROOT_DIR;
                break;
            case FILE_LIST_DIR:
                path = uri.getPath().substring(FileBrowserContract.File.TABLE_NAME.length() + 1);
                break;
            default:
                throw new IllegalArgumentException("Unsupported URI " + uri);
        }

        File dir = new File(path);
        String[] fileList = dir.list();

        if (projection == null) {
            projection = ALL_COLUMNS;
        }

        MatrixCursor cursor = new MatrixCursor(projection, fileList != null ? fileList.length : 1);

        if (!ROOT_DIR.equals(path)) {
            MatrixCursor.RowBuilder row = cursor.newRow();
            int lastIndex = path.lastIndexOf('/');

            for (String column : projection) {
                if (FileBrowserContract.File.Columns.TITLE.equals(column)) {
                    row.add("..");
                } else if (FileBrowserContract.File.Columns.PATH.equals(column)) {
                    row.add(path.substring(0, lastIndex == 0 ? 1 : lastIndex));
                } else if (FileBrowserContract.File.Columns.SIZE.equals(column)) {
                    row.add(0);
                } else if (FileBrowserContract.File.Columns.TYPE.equals(column)) {
                    row.add(FileBrowserContract.File.Type.DIRECTORY);
                } else {
                    row.add(null);
                }
            }
        }

        if (fileList != null) {
            for (String fileName : fileList) {
                MatrixCursor.RowBuilder row = cursor.newRow();
                File file = new File(path, fileName);

                for (String column : projection) {
                    if (FileBrowserContract.File.Columns.TITLE.equals(column)) {
                        row.add(fileName);
                    } else if (FileBrowserContract.File.Columns.PATH.equals(column)) {
                        row.add(file.getAbsolutePath());
                    } else if (FileBrowserContract.File.Columns.SIZE.equals(column)) {
                        row.add(file.length());
                    } else if (FileBrowserContract.File.Columns.TYPE.equals(column)) {
                        row.add(file.isDirectory() ? FileBrowserContract.File.Type.DIRECTORY
                                : FileBrowserContract.File.Type.FILE);
                    } else {
                        row.add(null);
                    }
                }
            }
        }

        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        switch (sUriMatcher.match(uri)) {
            case FILE_LIST_ROOT:
            case FILE_LIST_DIR:
                return "vnd.android.cursor.dir/filebrowser";
            default:
                throw new IllegalArgumentException("Unsupported URI " + uri);
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {
        // Not supported
        return null;
    }

    @Override
    public int delete(Uri uri, String s, String[] strings) {
        // Not supported
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues contentValues, String s, String[] strings) {
        // Not supported
        return 0;
    }
}
