
package com.example.sqlite;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.*;
import android.net.Uri;
import android.provider.BaseColumns;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class SQLiteProvider extends ContentProvider {

    private static final String TABLE_NAME = "table";

    private final IndexAllocator mIndexAllocator = new IndexAllocator();
    private final SQLiteDatabase mDatabase =
            SQLiteDatabase.create(new SQLiteDatabase.CursorFactory() {
                @Override
                public Cursor newCursor(SQLiteDatabase sqLiteDatabase,
                        SQLiteCursorDriver sqLiteCursorDriver, String editTable,
                        SQLiteQuery sqLiteQuery) {
                    return new SQLiteCursorImpl(sqLiteDatabase, sqLiteCursorDriver, editTable,
                            sqLiteQuery, mIndexAllocator);
                }
            });

    @Override
    public boolean onCreate() {
        return true;
    }

    @Override
    public Cursor query(Uri uri, final String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {
        String parameter = uri.getQueryParameter(SQLiteProviderContract.PARAM_URI);
        if (TextUtils.isEmpty(parameter)) {
            return null;
        }

        ContentResolver contentResolver = getContext().getContentResolver();
        Uri origUri = Uri.parse(parameter);

        Cursor rawCursor = doQuery(origUri, projection, selection, selectionArgs, sortOrder);
        if (rawCursor == null) {
            return null;
        }

        String[] queryColumns = rawCursor.getColumnNames();
        List<String> columnsList = Arrays.asList(queryColumns);
        if (columnsList.contains(BaseColumns._ID)) {
            String cursorType = contentResolver.getType(origUri);
            if (cursorType != null && cursorType.startsWith("vnd.android.cursor.item/")) {
                return rawCursor;
            }
        } else {
            // if the query was called with null projection we have to add
            // '_id' column because this content provider supports it
            if (projection == null) {
                List<String> columns = new ArrayList<String>(queryColumns.length + 1);
                columns.add(BaseColumns._ID);
                columns.addAll(columnsList);
                queryColumns = columns.toArray(new String[columns.size()]);
            }
        }

        try {
            List<ContentValues> values = new ArrayList<ContentValues>(rawCursor.getCount());
            int[] types = fillValues(rawCursor, values);

            long index = mIndexAllocator.acquireIndex();
            String tableName = TABLE_NAME + index;

            // create new table
            mDatabase.execSQL(getTableCommand(tableName, rawCursor, types));

            // fill the table with data
            for (ContentValues cv : values) {
                mDatabase.insert(tableName, null, cv);
            }

            // query data from the table
            SQLiteCursorImpl cursor = (SQLiteCursorImpl) mDatabase.query(tableName, queryColumns,
                    selection, selectionArgs, null, null, sortOrder);

            cursor.setTable(tableName, index);

            Uri notificationUri;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                notificationUri = rawCursor.getNotificationUri();
            } else {
                notificationUri = origUri;
            }

            // make sure our cursor is initialised with the same notification
            // Uri as the one obtained from 3-rd party content provider
            if (notificationUri != null) {
                cursor.setNotificationUri(contentResolver, notificationUri);
            }

            return cursor;
        } finally {
            rawCursor.close();
        }
    }

    private Cursor doQuery(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {
        ContentResolver contentResolver = getContext().getContentResolver();
        Cursor cursor;
        try {
            // try to make a query with original projection first
            cursor = contentResolver.query(uri, projection,
                    selection, selectionArgs, sortOrder);
        } catch (SQLiteException e) {
            String message = e.getMessage();
            if (message == null || !message.contains("no such column: _id")) {
                throw e;
            }

            // remove '_id' column from projection because it's not necessary
            // that
            // content provider support it
            if (projection != null) {
                List<String> columns = new ArrayList<String>(Arrays.asList(projection));
                columns.remove(BaseColumns._ID);
                projection = columns.toArray(new String[columns.size()]);
            }

            cursor = contentResolver.query(uri, projection,
                    selection, selectionArgs, sortOrder);
        }

        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        String parameter = uri.getQueryParameter(SQLiteProviderContract.PARAM_URI);
        if (TextUtils.isEmpty(parameter)) {
            throw new IllegalArgumentException("Unsupported URI " + uri);
        }
        return getContext().getContentResolver().getType(Uri.parse(parameter));
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }

    private static String getTableCommand(String tableName, Cursor cursor, int[] columnTypes) {
        StringBuilder sb = new StringBuilder("CREATE TABLE ");
        sb.append(tableName).append(" (_id INTEGER PRIMARY KEY AUTOINCREMENT");

        String[] queryColumns = cursor.getColumnNames();
        for (int i = 0; i < queryColumns.length; ++i) {
            if (BaseColumns._ID.equals(queryColumns[i])) {
                continue;
            }

            sb.append(", ").append(queryColumns[i]);

            switch (columnTypes[i]) {
                case Cursor.FIELD_TYPE_INTEGER:
                    sb.append(" INTEGER");
                    break;

                case Cursor.FIELD_TYPE_STRING:
                case Cursor.FIELD_TYPE_NULL:
                    sb.append(" TEXT");
                    break;

                case Cursor.FIELD_TYPE_FLOAT:
                    sb.append(" REAL");
                    break;

                case Cursor.FIELD_TYPE_BLOB:
                    sb.append(" BLOB");
                    break;
            }
        }
        sb.append(')');

        return sb.toString();
    }

    private static int[] fillValues(Cursor cursor, List<ContentValues> values) {
        String[] queryColumns = cursor.getColumnNames();
        int[] types = new int[queryColumns.length];
        Arrays.fill(types, Cursor.FIELD_TYPE_NULL);

        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
            ContentValues cv = new ContentValues(queryColumns.length);
            for (int i = 0; i < queryColumns.length; ++i) {
                int type = cursor.getType(i);
                switch (type) {
                    case Cursor.FIELD_TYPE_INTEGER:
                        cv.put(queryColumns[i], cursor.getLong(i));
                        break;

                    case Cursor.FIELD_TYPE_STRING:
                        cv.put(queryColumns[i], cursor.getString(i));
                        break;

                    case Cursor.FIELD_TYPE_FLOAT:
                        cv.put(queryColumns[i], cursor.getDouble(i));
                        break;

                    case Cursor.FIELD_TYPE_BLOB:
                        cv.put(queryColumns[i], cursor.getBlob(i));
                        break;

                    case Cursor.FIELD_TYPE_NULL:
                        cv.putNull(queryColumns[i]);
                        break;
                }

                if (type != Cursor.FIELD_TYPE_NULL) {
                    types[i] = type;
                }
            }
            values.add(cv);
        }

        return types;
    }

    private static class SQLiteCursorImpl extends SQLiteCursor {

        private final SQLiteDatabase mDatabase;
        private final IndexAllocator mIndexAllocator;
        private long mIndex;
        private String mTableName;

        private SQLiteCursorImpl(SQLiteDatabase db, SQLiteCursorDriver driver, String editTable,
                SQLiteQuery query, IndexAllocator indexAllocator) {
            super(driver, editTable, query);

            mDatabase = db;
            mIndexAllocator = indexAllocator;
        }

        public void setTable(String name, long index) {
            mTableName = name;
            mIndex = index;
        }

        @Override
        public void close() {
            super.close();
            mDatabase.execSQL("DROP TABLE IF EXISTS " + mTableName);
            mIndexAllocator.releaseIndex(mIndex);
        }
    }

    private static class IndexAllocator {
        private final List<Long> mIndexes = new LinkedList<Long>();

        private IndexAllocator() {
            mIndexes.add(0l);
        }

        public synchronized long acquireIndex() {
            long index = mIndexes.remove(0);
            if (mIndexes.isEmpty()) {
                mIndexes.add(index + 1);
            }
            return index;
        }

        public synchronized void releaseIndex(long index) {
            mIndexes.add(0, index);
        }
    }
}
